Good Morning,

This week's value is Frugality

"We do the most with the resources at hand, working to avoid waste and focusing on what’s most important."

Question of the day:

In which ways do we demonstrate frugality on our day to day? In which ways do we not? Identify specific attitudes and/or behaviors in each case.

¿De qué manera demostramos frugalidad en nuestro día a día? ¿De qué maneras no lo hacemos? Identifica las actitudes y / o comportamientos específicos en cada caso.

Thank you,
Amaya